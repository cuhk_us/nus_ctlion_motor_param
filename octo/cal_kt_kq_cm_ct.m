close all; clear; clc; 

%% We are using Air Gear 350 Kit
%  Motor: 2213, 920KV
%  Propeller: 9.5 x 4.5 inch
%  Using a 4 cell battery

g=9.781; 


%% estimate the CM and CT first based on Throtttle and RPM values 

% - from data sheet
% pwm   = [0.5, 0.65, 0.75, 0.85, 1]; % thottle percentage: [0, 1]
% omega = [4800, 6300, 7200, 8100, 8900]/60 * 2 * pi; % RPM / 60 * 2 * pi

% - measured
pwm   = ( [1200,     1300,   1400,   1500,   1600,   1700, 1800] - 1000 ) / 1000; % thottle percentage: [0, 1] 
omega =   [2445.2, 4059.8, 5575.7, 6958.1, 8204.6, 9282.2, 10218] / 60 * 2 * pi; % RPM / 60 * 2 * pi

pwm2omg = polyfit(pwm, omega, 1); 

CM = pwm2omg(1)
CT = pwm2omg(2)



%% estimte KT based on Thrust and RPM^2 values
omega2  = omega.^2; % omega^2

% - from data sheet
%thrust  = [278, 445, 568, 679, 813] / 1000 * g *0.8; % in N, 0.8 is due to coxial setup
%thrust  = [278, 445, 568, 679, 813] / 1000 * g ; % in N


% - from experiment
thrust  = [0.7648, 1.8261, 3.1506, 4.8489, 6.7599, 8.5657, 10.5054]; % in N

omg22thru = polyfit(omega2, thrust, 1); 

KT = omg22thru(1)


%% Compute KQ
% KQ = KT / 100  % Assume KQ is 100 times smaller thank KT

% - from experiment
torque  = [0.01118, 0.02431, 0.04321, 0.06585, 0.09064, 0.11068, 0.13990 ]; % in g * m

omg22torq = polyfit(omega2, torque, 1); 

KQ = omg22torq(1)