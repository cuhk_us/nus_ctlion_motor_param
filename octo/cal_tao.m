clc; clear; close all;

%% calcuate the time constant of the motor system
% Easy-to-remember points are ? @ 63%, 3? @ 95\% and 5? @ 99\%.

pathname = '.\'; % read motor test data on 28 August 2018

dataname        = 'Book2_rpm'; 
fullfilename    = [pathname, dataname, '.xlsx']; 
[num, txt, raw] = xlsread(fullfilename);

data0 = num(3:end, :);

data  = data0(~isnan(data0(:,3)),:); % remove nan

%% run time
t         = data(:,1); % time, s
rpm       = data(:,3); % rmp;

figure;
plot(t, rpm); title('t - rpm');
xlabel('time (s)'); ylabel('rpm'); grid on;